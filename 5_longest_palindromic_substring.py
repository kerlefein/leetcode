# https://leetcode.com/problems/longest-palindromic-substring/

# Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.

# Example 1:
# Input: "babad"
# Output: "bab"
# Note: "aba" is also a valid answer.

# Example 2:
# Input: "cbbd"
# Output: "bb"

class Solution:
    def longestPalindrome(self, s: str) -> str:
        if len(s) < 1:
            return s
        
        maxLength = 1
        
        start = 0 
        length = len(s)
        
        left, right = 0, 0
        
        for i in range(1, length):
            left = i - 1
            right = i
            while left >= 0 and right < length and s[left] == s[right]:
                if right - left + 1 > maxLength:
                    start = left
                    maxLength = right - left + 1
                left -= 1
                right += 1
                
            left = i - 1
            right = i + 1
            while left >= 0 and right < length and s[left] == s[right]:
                if right - left + 1 > maxLength:
                    start = left
                    maxLength = right - left + 1
                left -= 1
                right += 1
            
        return s[start:start+maxLength]