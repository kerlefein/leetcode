# https://leetcode.com/problems/shortest-unsorted-continuous-subarray/

# Given an integer array, you need to find one continuous subarray that if you only sort this subarray in ascending order, 
# then the whole array will be sorted in ascending order, too.
# You need to find the shortest such subarray and output its length.

# Example 1:
# Input: [2, 6, 4, 8, 10, 9, 15]
# Output: 5
# Explanation: You need to sort [6, 4, 8, 10, 9] in ascending order to make the whole array sorted in ascending order.

# Note:
# Then length of the input array is in range [1, 10,000].
# The input array may contain duplicates, so ascending order here means <=.

class Solution:
    def findUnsortedSubarray(self, nums: List[int]) -> int:
        if not nums:
            return 0
        first, last = -1, -1
        
        firstCheck = True
        for i in range(1, len(nums)):
            if nums[i-1] > nums[i]:
                if firstCheck:
                    first = i-1
                    firstCheck = False
                last = i
                
        if first == -1 and last == -1:
            return 0
        
        maxSub = max(nums[first:last+1])
        minSub = min(nums[first:last+1])

        for i in range(first):
            if nums[i] > minSub:
                first = i
                break
                
        i = len(nums) - 1
        while i > last:
            if nums[i] < maxSub:
                last = i
                break
            i -= 1
        result = last - first + 1

        return result