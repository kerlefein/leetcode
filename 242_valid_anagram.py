# https://leetcode.com/problems/valid-anagram/

# Given two strings s and t , write a function to determine if t is an anagram of s.

# Example 1:
# Input: s = "anagram", t = "nagaram"
# Output: true

# Example 2:
# Input: s = "rat", t = "car"
# Output: false

# Note: 
# You may assume the string contains only lowercase alphabets.

# Follow up:
# What if the inputs contain unicode characters? How would you adapt your solution to such case?

class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        from collections import defaultdict
        
        
        if len(s) != len(t):
            return False
        
        SS = defaultdict(int)
        TT = defaultdict(int)
        for i in s:
            SS[i] += 1
        for i in t:
            TT[i] += 1
        
        if SS == TT:
            return True
        return False