# https://leetcode.com/problems/move-zeroes/

# Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

# Example:
# Input: [0,1,0,3,12]
# Output: [1,3,12,0,0]

# Note:
# You must do this in-place without making a copy of the array.
# Minimize the total number of operations.

class Solution:
    def moveZeroes(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        Zcount = 0
        index = 0
        while index < len(nums):
            if nums[index] == 0:
                nums.remove(nums[index])
                Zcount += 1
            else:
                index += 1
        for i in range(Zcount):
            nums.append(0)
        