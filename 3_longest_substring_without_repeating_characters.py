# https://leetcode.com/problems/longest-substring-without-repeating-characters/

# Given a string, find the length of the longest substring without repeating characters.

# Example 1:

# Input: "abcabcbb"
# Output: 3 
# Explanation: The answer is "abc", with the length of 3. 
# Example 2:

# Input: "bbbbb"
# Output: 1
# Explanation: The answer is "b", with the length of 1.
# Example 3:

# Input: "pwwkew"
# Output: 3
# Explanation: The answer is "wke", with the length of 3. 
# Note that the answer must be a substring, "pwke" is a subsequence and not a substring.

class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        n = len(s)
        if n < 1:
            return 0
        elif n < 2:
            return 1
        
        length = 0
        strTemp = ''
        
        for i in range(n):
            strTemp = s[i]
            
            if i + 1 <= n:
                j = i+1
            else:
                break
            
            for j in range(i+1, n):
                if s[j] not in strTemp:
                    strTemp += s[j]
                else:
                    break
                
            length = max(length, len(strTemp))
            
        return length