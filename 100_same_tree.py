# https://leetcode.com/problems/same-tree/

# Given two binary trees, write a function to check if they are the same or not.
# Two binary trees are considered the same if they are structurally identical and the nodes have the same value.

# Example 1:
# Input:     1         1
#           / \       / \
#          2   3     2   3

#         [1,2,3],   [1,2,3]

# Output: true

# Example 2:
# Input:     1         1
#           /           \
#          2             2

#         [1,2],     [1,null,2]

# Output: false

# Example 3:
# Input:     1         1
#           / \       / \
#          2   1     1   2

#         [1,2,1],   [1,1,2]

# Output: false

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def isSameTree(self, p: TreeNode, q: TreeNode) -> bool:
        def preorder(root):
            return [root.val] + preorder(root.left) + preorder(root.right) if root else ['null']
        def inorder(root):
            return inorder(root.left) + [root.val] + inorder(root.right) if root else ['null']
        def postorder(root):
            return postorder(root.left) + postorder(root.right) + [root.val] if root else []
        if preorder(p) == preorder(q) and inorder(p) == inorder(q) and postorder(p) == postorder(q):
            return True
        return False