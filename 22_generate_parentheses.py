# https://leetcode.com/problems/generate-parentheses/

# Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

# For example, given n = 3, a solution set is:

# [
#   "((()))",
#   "(()())",
#   "(())()",
#   "()(())",
#   "()()()"
# ]

class Solution:
    def generateParenthesis(self, n: int) -> List[str]:
        if n == 0:
            return ['']
        if n == 1:
            return ['()']
        
        def generate(string, open, closed, n):
            if len(string) == n*2:
                result.append(string)
                return
            if open < n:
                generate(string + '(', open + 1, closed, n)
            if closed < open:
                generate(string + ')', open, closed + 1, n)

        
        result = []
        generate('', 0, 0, n)
        return result