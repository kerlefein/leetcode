# https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/

# Given an array of integers nums sorted in ascending order, find the starting and ending position of a given target value.
# Your algorithm's runtime complexity must be in the order of O(log n).
# If the target is not found in the array, return [-1, -1].

# Example 1:
# Input: nums = [5,7,7,8,8,10], target = 8
# Output: [3,4]

# Example 2:
# Input: nums = [5,7,7,8,8,10], target = 6
# Output: [-1,-1]

class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
        if not nums:
            return [-1, -1]
        
        low = 0
        high = len(nums) - 1
        while low < high:
            mid = (low + high) // 2
            if nums[mid] >= target:
                high = mid
            else:
                low = mid + 1
        if nums[high] != target:
            return [-1, -1]
        result = [high, high]

        low = 0
        high = result[0]
        while low < high:
            mid = (low + high) // 2
            if nums[mid] <= target - 1:
                low = mid + 1
            else:
                high = mid
        result[0] = high
        
        low = result[1]
        high = len(nums)
        while low < high:
            mid = (low + high) // 2
            if nums[mid] >= target + 1:
                high = mid
            else:
                low = mid + 1
        result[1] = high - 1
        
        return result
        