# https://leetcode.com/problems/valid-parentheses/

# Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

# An input string is valid if:

# Open brackets must be closed by the same type of brackets.
# Open brackets must be closed in the correct order.
# Note that an empty string is also considered valid.

# Example 1:
# Input: "()"
# Output: true

# Example 2:
# Input: "()[]{}"
# Output: true

# Example 3:
# Input: "(]"
# Output: false

# Example 4:
# Input: "([)]"
# Output: false

# Example 5:
# Input: "{[]}"
# Output: true

class Solution:
    def isValid(self, s: str) -> bool:
        pairsDict = {
            ']': '[',
            ')': '(',
            '}': '{',
        }
        if len(s) % 2 != 0:
            return False
        check = ''
        for i in s:
            if i == '(' or i == '[' or i == '{':
                check += i
            elif len(check) == 0:
                return False
            elif check[-1] == pairsDict[i]:
                check = check[:-1]
            else:
                return False
        if len(check) == 0:
            return True