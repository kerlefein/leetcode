# https://leetcode.com/problems/reverse-integer/

# Given a 32-bit signed integer, reverse digits of an integer.

# Example 1:
# Input: 123
# Output: 321

# Example 2:
# Input: -123
# Output: -321

# Example 3:
# Input: 120
# Output: 21

# Note: Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−2^31,  2^31 − 1]. 
# For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.

class Solution:
    def reverse(self, x: int) -> int:
        if x ==0:
            return x
        x = str(x)
        if x[0] == '-':
            x = x.replace('-', '')
            x = x[::-1]
            x = '-' + x
            return int(x) if -2**31 <= int(x) <= 2**31 -1 else 0
        x = x[::-1]
        while x[0] == 0:
            x = x.replace('0', '', 1)
        return int(x) if -2**31 <= int(x) <= 2**31 -1 else 0
    