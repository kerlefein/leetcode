# https://leetcode.com/problems/sort-an-array/

# Given an array of integers nums, sort the array in ascending order.

# Example 1:
# Input: nums = [5,2,3,1]
# Output: [1,2,3,5]

# Example 2:
# Input: nums = [5,1,1,2,0,0]
# Output: [0,0,1,1,2,5]
 
# Constraints:
# 1 <= nums.length <= 50000
# -50000 <= nums[i] <= 50000

# 1
class Solution1:
    def sortArray(self, nums: List[int]) -> List[int]:
        if len(nums) < 2:
            return nums
        
        pivot = nums[0]
        lesser = [x for x in nums if x < pivot]
        greater = [x for x in nums if x > pivot]
        equal = [x for x in nums if x == pivot]
        
        return self.sortArray(lesser) + equal + self.sortArray(greater)
    
# 2
class Solution:
    def sortArray(self, nums: List[int]) -> List[int]:
        if len(nums) > 1:
            mid = len(nums)//2
            left = nums[:mid]
            right = nums[mid:]
            
            self.sortArray(left)
            self.sortArray(right)

            i = j = k = 0
            while i < len(left) and j < len(right):
                if left[i] < right[j]:
                    nums[k] = left[i]
                    i += 1
                else:
                    nums[k] = right[j]
                    j += 1
                k += 1
                
            while i < len(left):
                nums[k] = left[i]
                i += 1
                k += 1
            while j < len(right):
                nums[k] = right[j]
                j += 1
                k += 1
                
        return nums