# https://leetcode.com/problems/maximum-subarray/

# Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

# Example:
# Input: [-2,1,-3,4,-1,2,1,-5,4],
# Output: 6
# Explanation: [4,-1,2,1] has the largest sum = 6.

# Follow up:
# If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle.

class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        if not nums:
            return 0
        
        tempSum = nums[0]
        maxSum = tempSum
        
        
        for i in range(1, len(nums)):
            if tempSum < 0 and nums[i] > tempSum:
                tempSum = nums[i]
            elif nums[i] > 0:
                tempSum += nums[i]
            elif nums[i] < 0:
                maxSum = max(tempSum, maxSum)
                tempSum += nums[i]
        return max(tempSum, maxSum)
    