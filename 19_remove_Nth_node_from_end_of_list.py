# https://leetcode.com/problems/remove-nth-node-from-end-of-list/

# Given a linked list, remove the n-th node from the end of list and return its head.

# Example:
# Given linked list: 1->2->3->4->5, and n = 2.
# After removing the second node from the end, the linked list becomes 1->2->3->5.

# Note: Given n will always be valid.

# Follow up: Could you do this in one pass?

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        dummy = head
        lenList = 0
        while dummy:
            lenList += 1
            dummy = dummy.next
        lenList -= n
        if lenList == 0:
            return head.next
        dummy = head
        while lenList != 1:
            lenList -= 1
            dummy = dummy.next
        dummy.next = dummy.next.next
        return head
    