# https://leetcode.com/problems/summary-ranges/

# Given a sorted integer array without duplicates, return the summary of its ranges.

# Example 1:
# Input:  [0,1,2,4,5,7]
# Output: ["0->2","4->5","7"]
# Explanation: 0,1,2 form a continuous range; 4,5 form a continuous range.

# Example 2:
# Input:  [0,2,3,4,6,8,9]
# Output: ["0","2->4","6","8->9"]
# Explanation: 2,3,4 form a continuous range; 8,9 form a continuous range.

class Solution:
    def summaryRanges(self, nums: List[int]) -> List[str]:
        arr = []
        temp = ''
        cons = 0
        if len(nums) > 0:
            temp += str(nums[0])
        if len(nums) == 1:
            arr.append(temp)
        for i in range(1,len(nums)):
            if (nums[i] - (nums[i-1])) > 1 and cons == 0:
                arr.append(temp)
                temp = str(nums[i])
            elif (nums[i] - (nums[i-1])) > 1 and cons > 0:
                temp += '->' + str(nums[i-1])
                arr.append(temp)
                temp = str(nums[i])
                cons = 0
            else:
                cons += 1
            if i == len(nums) - 1:
                if cons == 0:
                    arr.append(str(nums[i]))
                else:
                    temp += '->' + str(nums[i])
                    arr.append(temp)
        return arr
    