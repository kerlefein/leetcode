# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
#
# What is the 10 001st prime number?

primes = [2, 3]
a = 5
while len(primes) <= 10000:
    for i in range(2, a + 1):
        if a % i == 0 and a == i:
            primes.append(a)
            break
        elif a % i == 0:
            break
    a += 2
print(primes[-1])
