# где я так ошибся? 
# мой код исполняется несколько часов, а этот за 7.376072406768799 секунд

import time
import math

n = int(input("Please Enter A Positive Number: "))
start=time.time()
prime=[2]

for num in range(3,n,2):
    if all(num % i !=0 for i in range(3,int(math.sqrt(num))+1, 2)):
       prime.append(num)

S=0
for value in prime:
    S=S+value

print("The Sum Of All Primes Below ",n," Is = ", S)
print("Execution Time In Seconds = " ,(time.time() - start))

# мой код номер 1:
# ==========================================
# from math import sqrt
#
#
# end = 2000000
# ans = 2
# for val in range(3, end + 1, 2):
#     for n in range(2, val):
#         if (val % n) == 0:
#             break
#     else:
#         ans += val
# print(ans)
# ==========================================
# мой код номер 2:
# после анализа матчасти
# результат 6.263023853302002
# ==========================================
# import math
# import time
#
# end = 2000000
# ans = 2
# start = time.time()
# for val in range(3, end + 1, 2):
#     for n in range(3, int(math.sqrt(val)) + 1, 2):
#         if (val % n) == 0:
#             break
#     else:
#         ans += val
# print(ans)
# print('Time:', (time.time() - start))
# ==========================================
