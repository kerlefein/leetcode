# мой франкенштейн

DIC = {
    '1': 3,
    '2': 3,
    '3': 5,
    '4': 4,
    '5': 4,
    '6': 3,
    '7': 5,
    '8': 5,
    '9': 4,
    '10': 3,
    '11': 6,
    '12': 6,
    '13': 8,
    '14': 8,
    '15': 7,
    '16': 7,
    '17': 9,
    '18': 8,
    '19': 8,
    '20': 6,
    '30': 6,
    '40': 5,
    '50': 5,
    '60': 5,
    '70': 7,
    '80': 6,
    '90': 6,
    '100': 7,
    '1000': 8,
}
cnt = 0
def word(num):
    ls = list(str(num))
    if num < 21:
        ans = DIC[str(num)]
    elif 20 < num < 100:
        if int(ls[1]) == 0:
            a = int(ls[0]) * 10
            ans = DIC[str(a)]
        else:
            a = int(ls[0]) * 10
            ans = DIC[str(a)] + DIC[ls[1]]
    elif 99 < num < 1000:
        if 0 < int(ls[1] + ls[2]) < 21:
            a = int(ls[1] + ls[2])
            ans = DIC[ls[0]] + DIC['100'] + 3 + DIC[str(a)]
        else:
            if int(ls[2]) == 0 and int(ls[1]) == 0:
                ans = DIC[ls[0]] + DIC['100']
            elif int(ls[2]) == 0:
                a = int(ls[1]) * 10
                ans = DIC[ls[0]] + DIC['100'] + 3 + DIC[str(a)]
            else:
                a = int(ls[1]) * 10
                ans = DIC[ls[0]] + DIC['100'] + 3 + DIC[str(a)] + DIC[ls[2]]
    else:
        ans = DIC['1'] + DIC['1000']
    return(ans)
for i in range(1, 1001):
    cnt += word(i)
print(cnt)

# короче еще вот так можно было лол
# import inflect
# ig = inflect.engine()
# s=0
# for i in range(1,1001):
#     s+=len(ig.number_to_words(i).replace(' ','').replace('-',''))
# print(s)
