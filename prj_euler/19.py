y = 1901
m = 0
d = 1
count = 0

months_common = {
    0: 31,
    1: 28,
    2: 31,
    3: 30,
    4: 31,
    5: 30,
    6: 31,
    7: 31,
    8: 30,
    9: 31,
    10: 30,
    11: 31,
    }

months_leap = {
    0: 31,
    1: 29,
    2: 31,
    3: 30,
    4: 31,
    5: 30,
    6: 31,
    7: 31,
    8: 30,
    9: 31,
    10: 30,
    11: 31,
    }


while y <= 2000:
    if y % 4 != 0:
        while m <= 11:
            if (d+1) % 7 == 0:
                count += 1
            d += months_common[m]
            m += 1
    elif y % 100 != 0:
        while m <= 11:
            if (d+1) % 7 == 0:
                count += 1
            d += months_leap[m]
            m += 1
    elif y % 400 != 0:
        while m <= 11:
            if (d+1) % 7 == 0:
                count += 1
            d += months_common[m]
            m += 1
    else:
        while m <= 11:
            if (d+1) % 7 == 0:
                count += 1
            d += months_leap[m]
            m += 1
    y += 1
    m = 0
print(count)
