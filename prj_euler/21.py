import math


dic = {}
answ = 0
for num in range(1, 10001):
    S = 0
    for i in range(1, math.ceil(num/2) + 1):
        if num % i == 0:
            S += i
    if S in dic and num == dic[S]:
        answ += S
        answ += num
    dic[num] = S
print(answ)
