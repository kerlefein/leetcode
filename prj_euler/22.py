ans = 0
dic = {
    'A': 1,
    'B': 2,
    'C': 3,
    'D': 4,
    'E': 5,
    'F': 6,
    'G': 7,
    'H': 8,
    'I': 9,
    'J': 10,
    'K': 11,
    'L': 12,
    'M': 13,
    'N': 14,
    'O': 15,
    'P': 16,
    'Q': 17,
    'R': 18,
    'S': 19,
    'T': 20,
    'U': 21,
    'V': 22,
    'W': 23,
    'X': 24,
    'Y': 25,
    'Z': 26,
}

a = open(r'/home/kerlefein/Code/prj Euler/files/p022_names.txt', 'r').read()
a = a.split(',')
for i in range(len(a)):
    a[i] = a[i].replace('\"', '')
a.sort()
for name in a:
    Sch = 0
    for ch in name:
        Sch += dic[ch]
    ans += Sch * (a.index(name) + 1)
print(ans)
