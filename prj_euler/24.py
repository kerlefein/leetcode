from math import factorial


P = 999999
a = '0123456789'
b = ''
for i in range(10, 0, -1):
    perm = P // factorial(i-1)
    b += a[perm]
    a = a.replace(a[perm], '')
    P = P % factorial(i-1)
print(b)
