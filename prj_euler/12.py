# а получилось неплохо

def div_num(factor):
    x, d = 2, 1
    div, cnt = 1, 0
    while x != factor:
        if factor % x == 0:
            factor = factor // x
            cnt += 1
        elif factor % (x + d) == 0 and cnt != 0:
            div = div * (cnt + 1)
            cnt = 0
        else:
            x += d
            d = 2
    cnt += 1
    div = div * (cnt + 1)        
    return (div)

triangle_num = 3
next_num = 3

while div_num(triangle_num) <= 500:
    triangle_num += next_num

    next_num += 1
print(triangle_num)
