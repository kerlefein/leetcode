import math


def abundant(n):
    divSum = 0
    for i in range(1, math.ceil(n/2) + 1):
        if n % i == 0:
            divSum += i
    if divSum > n:
        return(n)

abundantList = []
for j in range(28123):
    a = abundant(j)
    if a is not None:
        abundantList.append(a)

sumAbList = []
for k in range(len(abundantList)):
    for l in range(k, len(abundantList)):
        if abundantList[k] + abundantList[l] <= 28123:
            sumAbList.append(abundantList[k] + abundantList[l])

# эту часть можно было бы ускорить, чтобы поиск шел не по всему списку
sumNotAb = 0                    
for c in range(1, 28123):
    if c not in sumAbList:
        sumNotAb += c

print(sumNotAb)
