# это не мое решение
def LP_factor(factor):
    x, d = 2, 1
    while x * x <= factor:
        if factor % x == 0:
            factor = factor // x
        else:
            x += d
            d = 2
    return (factor)


print(LP_factor(600851475143))
