from math import factorial


fact100 = factorial(100)
ls = [int(x) for x in str(fact100)]
ans = 0
for i in range(len(ls)):
    ans += ls[i]
print(ans)
