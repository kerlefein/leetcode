# https://leetcode.com/problems/3sum/

# Given an array nums of n integers, are there elements a, b, c in nums such that a + b + c = 0? 
# Find all unique triplets in the array which gives the sum of zero.

# Note: The solution set must not contain duplicate triplets.

# Example:
# Given array nums = [-1, 0, 1, 2, -1, -4],

# A solution set is:
# [
#   [-1, 0, 1],
#   [-1, -1, 2]
# ]

class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        result = set()
        dic = {}
        for n in nums:            
            dic[n] = dic.get(n, 0) + 1
            
        nums = sorted([k for k, v in dic.items()])
        for i, n1 in enumerate(nums): 
            for n2 in nums[i:]:
                if (n1 == n2) and (dic[n1] < 2):
                    continue
                        
                n3 = 0 - n1 - n2
                if n3 in dic:
                    count = dic[n3]
                    if n1 == n3:
                        count -= 1
                    if n2 == n3:
                        count -= 1
                    if count > 0:                        
                        result.add(tuple(sorted([n1, n2, n3])))
        return result